/*
  Aŭtorrajto © 2023-2024 Arturo Teslao
  Ĉi tiu verko estas disponebla laŭ la permesilo GNUa GPLv2, Ĝenerala Publika Permesilo de GNU Tutmonda versio 2.
  https://www.gnu.org/licenses/gpl-2.0.html

  Copyright © 2023-2024 Arthur Tesla
  This work is licensed under a GNU GPLv2, GNU General Public License (GNU GPLv2).
  https://www.gnu.org/licenses/gpl-2.0.html
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <limits.h>

#define MY_FILE_USERS "uzantoj"
#define MY_BALANCE *My_balance_and_bet
#define MY_BET *(My_balance_and_bet + 1)
#define MY_SLOT_0 *My_slots
#define MY_SLOT_1 *(My_slots + 1)
#define MY_SLOT_2 *(My_slots + 2)

char My_user_name[16];
unsigned long int My_balance_and_bet[2] = {0};  /* Bilanco kaj vetaĵo */

int My_user()
  {
    printf("Skribu cian nomon: ");
    scanf("%s", My_user_name);

    MY_BALANCE = 100;
  }

int My_slot_machine(void)
  {
    unsigned short int My_slots[3] = {0, 0, 0};  /* slots */
    FILE *fp;
    printf("Saluton %s!\n", My_user_name);
    puts("\n\n"
    "Por eliri, uzu veton egalantan al 0\n\n"
    "Tri egalaj ciferoj 0 0 0 estas profito egalas vataĵo multiplikis per cent.\n"
    "Tri egalaj ciferoj 1 1 1 estas profito egalas vataĵo multiplikis per tri.\n"
    "Tri egalaj ciferoj 2 2 2 estas profito egalas vataĵo multiplikis per ses.\n"
    "Tri egalaj ciferoj 3 3 3 estas profito egalas vataĵo multiplikis per naŭ.\n"
    "Tri egalaj ciferoj 4 4 4 estas profito egalas vataĵo multiplikis per dec du.\n"
    "Tri egalaj ciferoj 5 5 5 estas profito egalas vataĵo multiplikis per dec kvin.\n"
    "Tri egalaj ciferoj 6 6 6 estas profito egalas vataĵo multiplikis per dek ok.\n"
    "Tri egalaj ciferoj 7 7 7 estas profito egalas vataĵo multiplikis per dudek unu.\n"
    "Tri egalaj ciferoj 8 8 8 estas profito egalas vataĵo multiplikis per dudec kvar.\n"
    "Tri egalaj ciferoj 9 9 9 estas profito egalas vataĵo multiplikis per dudec sep.\n"
    "Du egalaj ciferoj estas profito unu.\n"
    "Neniuj egalaj ciferoj estas nur perdo de vetaĵo.\n");

    while(MY_BALANCE > 0)
      {
        printf("Cia bilanco estas %llu\nCia veto: ", MY_BALANCE);

        if (scanf("%lu", &MY_BET) != 1)
          {
            puts("Malkorekta vetaĵo!");
            while (getchar() != '\n');
            continue;
          }
        if(MY_BET == 0)
          {
            printf("Cia bilanco estas %lu\nAdiaŭ!\n", MY_BALANCE);
            return 0;
          }
        if(MY_BET > MY_BALANCE || MY_BET < 0)
          {
            puts("Malkorekta vetaĵo!");
            continue;
          }
        else
          {
            fp = fopen("/dev/urandom", "r");
            if(fp == NULL)
              {
                perror("Error opening /dev/urandom");
                return 1;
              }
            fread(&MY_SLOT_0, sizeof(unsigned short int), 1, fp);
            fread(&MY_SLOT_1, sizeof(unsigned short int), 1, fp);
            fread(&MY_SLOT_2, sizeof(unsigned short int), 1, fp);
            fclose(fp);
            MY_SLOT_0 %= 10;
            MY_SLOT_1 %= 10;
            MY_SLOT_2 %= 10;
            printf("%u %u %u\n", MY_SLOT_0, MY_SLOT_1, MY_SLOT_2);
            /* A-A-A */
            if(MY_SLOT_0 == MY_SLOT_1 && MY_SLOT_1 == MY_SLOT_2)
              {
                if(MY_SLOT_0 == 0)  /* 0 0 0 */
                  {
                    if((MY_BALANCE + MY_BET * MY_BET - MY_BET) >= ULONG_MAX)
                      {
                        MY_BALANCE = ULONG_MAX;
                        printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                      }
                    else
                      {
                        MY_BALANCE = (MY_BALANCE + MY_BET * MY_BET - MY_BET); /* my_balance + my_bet * my_bet - my_bet + 0 * 0 * 0 */
                        printf("Ci profitis %lu\n", MY_BET * MY_BET);
                      }
                  }
                if(MY_SLOT_0 == 1)  /* 1 1 1 */
                  {
                    if((MY_BALANCE + MY_BET * MY_BET - MY_BET) >= ULONG_MAX)
                      {
                        MY_BALANCE = ULONG_MAX;
                        printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                      }
                    else
                      {
                        MY_BALANCE = (MY_BALANCE + MY_BET * MY_BET - MY_BET + 1); /* my_balance + my_bet * my_bet - my_bet + 1 * 1 * 1 */
                        printf("Ci profitis %lu\n", MY_BET * MY_BET + 1);
                      }
                  }
                if(MY_SLOT_0 == 2)  /* 2 2 2 */
                  {
                    if((MY_BALANCE + MY_BET * MY_BET - MY_BET) >= ULONG_MAX)
                      {
                        MY_BALANCE = ULONG_MAX;
                        printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                      }
                    else
                      {
                        MY_BALANCE = (MY_BALANCE + MY_BET * MY_BET - MY_BET + 8); /* my_balance + my_bet * my_bet - my_bet + 2 * 2 * 2 */
                        printf("Ci profitis %lu\n", MY_BET * MY_BET + 8);
                      }
                  }
                if(MY_SLOT_0 == 3)  /* 3 3 3 */
                  {
                    if((MY_BALANCE + MY_BET * MY_BET - MY_BET) >= ULONG_MAX)
                      {
                        MY_BALANCE = ULONG_MAX;
                        printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                      }
                    else
                      {
                        MY_BALANCE = (MY_BALANCE + MY_BET * MY_BET - MY_BET + 27); /* my_balance + my_bet * my_bet - my_bet + 3 * 3 * 3 */
                        printf("Ci profitis %lu\n", MY_BET * MY_BET + 27);
                      }
                  }
                if(MY_SLOT_0 == 4)  /* 4 4 4 */
                  {
                    if((MY_BALANCE + MY_BET * MY_BET - MY_BET) >= ULONG_MAX)
                      {
                        MY_BALANCE = ULONG_MAX;
                        printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                      }
                    else
                      {
                        MY_BALANCE = (MY_BALANCE + MY_BET * MY_BET - MY_BET + 64); /* my_balance + my_bet * my_bet - my_bet + 4 * 4 * 4 */
                        printf("Ci profitis %lu\n", MY_BET * MY_BET + 64);
                      }
                  }
                if(MY_SLOT_0 == 5)  /* 5 5 5 */
                  {
                    if((MY_BALANCE + MY_BET * MY_BET - MY_BET) >= ULONG_MAX)
                      {
                        MY_BALANCE = ULONG_MAX;
                        printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                      }
                    else
                      {
                        MY_BALANCE = (MY_BALANCE + MY_BET * MY_BET - MY_BET + 125); /* my_balance + my_bet * my_bet - my_bet + 5 * 5 * 5 */
                        printf("Ci profitis %lu\n", MY_BET * MY_BET + 125);
                      }
                  }
                if(MY_SLOT_0 == 6)  /* 6 6 6 */
                  {
                    if((MY_BALANCE + MY_BET * MY_BET - MY_BET) >= ULONG_MAX)
                      {
                        MY_BALANCE = ULONG_MAX;
                        printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                      }
                    else
                      {
                        MY_BALANCE = (MY_BALANCE + MY_BET * MY_BET - MY_BET + 216); /* my_balance + my_bet * my_bet - my_bet + 6 * 6 * 6 */
                        printf("Ci profitis %lu\n", MY_BET * MY_BET + 216);
                      }
                  }
                if(MY_SLOT_0 == 7)  /* 7 7 7 */
                  {
                    if((MY_BALANCE + MY_BET * MY_BET - MY_BET) >= ULONG_MAX)
                      {
                        MY_BALANCE = ULONG_MAX;
                        printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                      }
                    else
                      {
                        MY_BALANCE = (MY_BALANCE + MY_BET * MY_BET - MY_BET + 343); /* my_balance + my_bet * my_bet - my_bet + 7 * 7 * 7 */
                        printf("Ci profitis %lu\n", MY_BET * MY_BET + 343);
                      }
                  }
                if(MY_SLOT_0 == 8)  /* 8 8 8 */
                  {
                    if((MY_BALANCE + MY_BET * MY_BET - MY_BET) >= ULONG_MAX)
                      {
                        MY_BALANCE = ULONG_MAX;
                        printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                      }
                    else
                      {
                        MY_BALANCE = (MY_BALANCE + MY_BET * MY_BET - MY_BET + 512); /* my_balance + my_bet * my_bet - my_bet + 8 * 8 * 8 */
                        printf("Ci profitis %lu\n", MY_BET * MY_BET + 512);
                      }
                  }
                if(MY_SLOT_0 == 9)  /* 9 9 9 */
                  {
                    if((MY_BALANCE + MY_BET * MY_BET - MY_BET) >= ULONG_MAX)
                      {
                        MY_BALANCE = ULONG_MAX;
                        printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                      }
                    else
                      {
                        MY_BALANCE = (MY_BALANCE + MY_BET * MY_BET - MY_BET + 729); /* my_balance + my_bet * my_bet - my_bet + 9 * 9 * 9 */
                        printf("Ci profitis %lu\n", MY_BET * MY_BET + 729);
                      }
                   }
              }
            /* A-A-B */
            if((MY_SLOT_0 == MY_SLOT_1 && MY_SLOT_1 != MY_SLOT_2) || (MY_SLOT_0 != MY_SLOT_1 && MY_SLOT_1 == MY_SLOT_2) || (MY_SLOT_0 != MY_SLOT_1 && MY_SLOT_0 == MY_SLOT_2))
              {
                if((MY_BALANCE + MY_BET * 2 - MY_BET) >= ULONG_MAX)
                  {
                    MY_BALANCE = ULONG_MAX;
                    printf("Cia bilanco ne povas esti pli granda ol %lu\n", MY_BALANCE);
                  }
                else
                  {
                    MY_BALANCE = (MY_BALANCE + MY_BET * 2 - MY_BET); /* my_balance + my_bet * 2 - my_bet */
                    printf("Ci profitis %lu\n", MY_BET * 2);
                  }
              }
            /* A-B-C */
            if(MY_SLOT_0 != MY_SLOT_1 && MY_SLOT_0 != MY_SLOT_2 && MY_SLOT_1 != MY_SLOT_2)
              {
                MY_BALANCE = (MY_BALANCE - MY_BET);  /* my_balance - my_bet */
                puts("Ci profitis 0");
              }
          }  /* else */
      }  /* while(my_balance > 0)  */
    printf("Cia bilanco estas %lu\nAdiaŭ!\n", MY_BALANCE);
    return 0;
  }  /* int My_slot_machine(void) */

int main(void)
  {
     My_user();
     My_slot_machine();

     return 0;
  }
